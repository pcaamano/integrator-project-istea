﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace DataAccessLayer.Memory{

    public class DALCliente : IDALCliente{

        private static List<Cliente> clientes = new List<Cliente>();

        /// <summary>
        /// Metodo para llamar el metodo que borra entidades.
        /// </summary>
        /// <param name="id">idCliente</param>
        public void Delete(int? id) {
            this.Delete(id.Value);
        }

        /// <summary>
        /// Método para borrar un cliente por id
        /// </summary>
        /// <param name="id">idCliente</param>
        public void Delete(int id) {
            ValidarDelete(id);

            // Instancia para guardar cliente buscado
            Cliente cliEliminar = new Cliente();

            // Busco el cliente a eliminar
            foreach (Cliente cliente in clientes)
            {
                if (cliente.Id.Value.Equals(id))
                {
                    cliEliminar = cliente;

                }
            }

            // Elimino el cliente encontrado
            clientes.Remove(cliEliminar);
        }

        private void ValidarDelete(int idCliente){
            int idExistente = 0;

            foreach(Cliente cliente in clientes){
                if (cliente.Id.Value == idCliente)
                    idExistente = idCliente;
            }

            if (idExistente == 0)
                throw new Exception();
        }

        /// <summary>
        /// Metodo para vaciar todos los clientes
        /// </summary>
        public void DeleteAll() {
            clientes.Clear();
        }

        /// <summary>
        /// Metodo para insertar una entidad
        /// </summary>
        /// <param name="cliente">Cliente</param>
        public void Insert(Cliente cliente){
            // validar cliente a insertar, si existe se arrojara unaa excepcion
            ValidacionInsert(cliente.DNI);

            // genero id
            int idEntidad = GetNewId();

            // asigno id a la entidad
            cliente.Id = idEntidad;

            if(cliente != null)
                clientes.Add(cliente);
        }

        /// <summary>
        /// Metodo para validar si la entidad a insertar existe
        /// </summary>
        /// <param name="dni"></param>
        private void ValidacionInsert(int dni){
            foreach(Cliente cliente in clientes){
                if (cliente.DNI == dni)
                    throw new DNIRepetidoException();
            }
        }

        /// <summary>
        /// Metodo que permite encontrar un ultimo id e incrementarlo para asignarlo a una nueva entidad
        /// </summary>
        /// <returns></returns>
        private int GetNewId(){
            int id = 0;

            foreach(Cliente cliente in clientes){
                if (cliente.Id.Value > id)
                    id = cliente.Id.Value;
            }

            return id++;
        }

        /// <summary>
        /// Retorna una lista con todos los clientes
        /// </summary>
        /// <returns>List<CLiente></returns>
        public List<Cliente> List(){
            return clientes;
        }

        /// <summary>
        /// Lista los clientes que coinciden con el apellido recibido por parametro
        /// </summary>
        /// <param name="apellido"></param>
        /// <returns>List<Cliente></Cliente></returns>
        public List<Cliente> List(string apellido) {
            List<Cliente> buscados = new List<Cliente>();

            foreach(Cliente cliente in clientes){
                if (cliente.Apellido.Equals(apellido)){
                    buscados.Add(cliente);
                }
            }

            return buscados;
        }


        /// <summary>
        /// Metodo para buscar Clientes
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Cliente Load(int? id){
            throw new NotImplementedException();
        }

        public Cliente Load(int id){
            throw new NotImplementedException();
        }

        /// <summary>
        /// Metodo que permite guardar la entidad.
        /// Si esta no existe la guarda como una nueva, en caso de existir la actualiza.
        /// </summary>
        /// <param name="cliente"></param>
        public void Save(Cliente cliente){
            if (!cliente.Id.HasValue)
                this.Insert(cliente);
            else
                this.Update(cliente);
        }

        public void Update(Cliente cliente)
        {
            throw new NotImplementedException();
        }
    }
}
