﻿using System;
using DataAccessLayer;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        private IDALCliente _IDALCliente { set; get; }

        [TestInitialize()]
        public void Initialize()
        {
            //_IDALCliente = new DataAccessLayer.DataBase.DALCliente();
            //_IDALCliente = new DataAccessLayer.FileSystem.DALCliente();  
            _IDALCliente = new DataAccessLayer.Memory.DALCliente();

            _IDALCliente.DeleteAll();
        }
        
        /// <summary>
        /// Genera un nuevo cliente.
        /// </summary>
        /// <returns></returns>
        private Cliente GetNewCliente()
        {
            Cliente cliente = new Cliente();
            cliente.DNI = 1234567890;
            cliente.Apellido = "Sadosky";
            cliente.CantidadAdeudada = 10;
            return cliente;
        }

        [TestMethod]
        public void Save()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            #endregion

            #region Act (perform the actual work of the test)
            _IDALCliente.Save(cliente);
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(true, cliente.Id.HasValue);
            #endregion
        }

        [TestMethod]
        public void Update()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            _IDALCliente.Save(cliente);
            cliente.DNI = 1;
            cliente.Apellido = "Leloir";
            cliente.CantidadAdeudada = 100;
            #endregion

            #region Act (perform the actual work of the test)
            _IDALCliente.Update(cliente);
            #endregion

            #region Assert (verify the result)
            Cliente clienteAux = _IDALCliente.Load(cliente.Id);
            Assert.AreEqual(cliente.Id.Value, clienteAux.Id.Value);
            Assert.AreEqual(1, clienteAux.DNI);
            Assert.AreEqual("Leloir", clienteAux.Apellido);
            Assert.AreEqual(100, clienteAux.CantidadAdeudada);
            #endregion
        }

        [TestMethod]
        public void Delete()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            _IDALCliente.Save(cliente);
            #endregion

            #region Act (perform the actual work of the test)
            int cantAntes = _IDALCliente.List().Count;
            _IDALCliente.Delete(cliente.Id.Value); //Sobrecarga con parametro entero
            int cantDespues = _IDALCliente.List().Count;
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(1, cantAntes);
            Assert.AreEqual(0, cantDespues);
            #endregion
        }

        [TestMethod]
        public void DeleteOverloading()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            _IDALCliente.Save(cliente);
            #endregion

            #region Act (perform the actual work of the test)
            int cantAntes = _IDALCliente.List().Count;
            _IDALCliente.Delete(cliente.Id); //Sobrecarga con parametro entero nuleable
            int cantDespues = _IDALCliente.List().Count;
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(1, cantAntes);
            Assert.AreEqual(0, cantDespues);
            #endregion
        }

        [TestMethod]
        public void Insert()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            #endregion

            #region Act (perform the actual work of the test)
            int cantAntes = _IDALCliente.List().Count;
            _IDALCliente.Insert(cliente);
            int cantDespues = _IDALCliente.List().Count;
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(0, cantAntes);
            Assert.AreEqual(1, cantDespues);
            #endregion
        }

        [TestMethod]
        public void List()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            const int TOTAL = 3;
            for (int i = 0; i < TOTAL; i++)
            {
                Cliente cliente = this.GetNewCliente();
                cliente.DNI += i; //Incremento el valor del DNI ya que es clave sin repetidos (unique key) 
                _IDALCliente.Insert(cliente);
            }
            #endregion

            #region Act (perform the actual work of the test)
            int totalClientes = _IDALCliente.List().Count;
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(TOTAL, totalClientes);
            #endregion
        }

        [TestMethod]
        public void ListWithFilter()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            const int TOTAL = 3;
            for (int i = 0; i < TOTAL; i++)
            {
                Cliente cliente = this.GetNewCliente();
                cliente.DNI = i; //Clave sin duplicados (index unique)
                cliente.Apellido = string.Format("Balseiro {0}", i);
                _IDALCliente.Insert(cliente);
            }
            string apellidoBuscar = string.Format("Balseiro {0}", 2);
            #endregion

            #region Act (perform the actual work of the test)
            List<Cliente> clientesEncontrados = _IDALCliente.List(apellidoBuscar);
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(1, clientesEncontrados.Count);
            Assert.AreEqual(apellidoBuscar, clientesEncontrados[0].Apellido);
            #endregion
        }

        [TestMethod]
        public void Load()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            _IDALCliente.Save(cliente);
            _IDALCliente.Delete(cliente.Id);
            bool testExitoso = false;
            #endregion

            #region Act (perform the actual work of the test)
            try
            {
                //Se espera que el método lance una excepción
                _IDALCliente.Load(cliente.Id.Value);
                //Test fallido
                testExitoso = false;
            }
            catch
            {
                //Test exitoso
                testExitoso = true;
            }
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(true, testExitoso);
            #endregion
        }

        [TestMethod]
        public void LoadOverloading()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            _IDALCliente.Save(cliente);
            _IDALCliente.Delete(cliente.Id);
            bool testExitoso = false;
            #endregion

            #region Act (perform the actual work of the test)
            try
            {
                //Se espera que el método lance una excepción
                _IDALCliente.Load(cliente.Id);
                //Test fallido
                testExitoso = false;
            }
            catch
            {
                //Test exitoso
                testExitoso = true;
            }
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(true, testExitoso);
            #endregion
        }

        [TestMethod]
        public void ValidateConstraintInsert()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente cliente = this.GetNewCliente();
            _IDALCliente.Save(cliente);
            #endregion

            #region Act (perform the actual work of the test)
            bool testExitoso;
            try
            {
                cliente.Id = null; //Se establece en null el Id para que se tome como un nuevo cliente.
                //Se inserta un cliente con el mismo DNI para generar un error, por valor duplicado.
                _IDALCliente.Insert(cliente);
                //Test fallido
                testExitoso = false;
            }
            catch (DNIRepetidoException e)
            {
                //Test exitoso
                testExitoso = true;
            }
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(true, testExitoso);
            #endregion
        }

        [TestMethod]
        public void ValidateConstraintUpdate()
        {
            #region Arrange (setup the testing objects and prepare the prerequisites for your test)
            Cliente clienteA = this.GetNewCliente();
            _IDALCliente.Save(clienteA);

            Cliente clienteB = this.GetNewCliente();
            clienteB.DNI++;
            _IDALCliente.Save(clienteB);
            #endregion

            #region Act (perform the actual work of the test)
            bool testExitoso;
            try
            {
                Cliente clienteC = this.GetNewCliente();
                clienteC.Id = clienteB.Id;
                //Se inserta un cliente con el mismo DNI para generar un error, por valor duplicado.
                _IDALCliente.Update(clienteC);
                //Test fallido
                testExitoso = false;
            }
            catch (DNIRepetidoException)
            {
                //Test exitoso
                testExitoso = true;
            }
            #endregion

            #region Assert (verify the result)
            Assert.AreEqual(true, testExitoso);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ClienteNoEncontradoException))]
        public void ValidateLoadClienteNoEncontradoException()
        {
            #region Act (perform the actual work of the test)
            _IDALCliente.Load(100);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ClienteNoEncontradoException))]
        public void ValidateDeleteClienteNoEncontradoException()
        {
            #region Act (perform the actual work of the test)
            _IDALCliente.Delete(100);
            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ClienteNoEncontradoException))]
        public void ValidateUpdateClienteNoEncontradoException()
        {
            #region Act (perform the actual work of the test)
            Cliente cliente = new Cliente();
            /*Se crea un id que no existe a los fines de causar un error 
            y poder realizar la prueba.*/
            cliente.Id = 100;
            cliente.Apellido = "Sabato";
            cliente.DNI = 1234567;
            cliente.CantidadAdeudada = 0;
            #endregion

            #region Act (perform the actual work of the test)
            _IDALCliente.Update(cliente);
            #endregion
        }
    }
}
