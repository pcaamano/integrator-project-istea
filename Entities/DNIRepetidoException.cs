﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    /// <summary>
    /// Representa un error por número de DNI ya existente.
    /// </summary>
    public class DNIRepetidoException : Exception { }
}