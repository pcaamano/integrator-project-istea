﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public interface IDALCliente
    {
        void Save(Cliente cliente);
        void Update(Cliente cliente);
        void Delete(int id);
        void Delete(int? id);
        void DeleteAll();
        void Insert(Cliente cliente);
        List<Cliente> List();
        List<Cliente> List(string apellido);
        Cliente Load(int id);
        Cliente Load(int? id);
    }
}
