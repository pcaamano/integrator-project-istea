﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Cliente
    {
        public int? Id { set; get; }
        public int DNI { set; get; }
        public string Apellido { set; get; }
        public int CantidadAdeudada { set; get; }
    }
}
